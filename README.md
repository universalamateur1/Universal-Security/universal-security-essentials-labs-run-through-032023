# **GitLab Security Essentials Hands-On Guide**

## Template to be used for this lab

* [Security Essentials Labs](https://spt.gitlabtraining.cloud/professional-services-classes/templates/security-essentials-labs)

## **GitLab Security Essentials Labs**

* [Lab 1: Enable, configure, and run SAST, Secret Detection, and DAST](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/secessentialshandson1.html)
* [Lab 2: Enable, configure, and run Dependency Scanning](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/secessentialshandson2.html)
* [Lab 3: Enable, configure, and run Container Scanning](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/secessentialshandson3.html)
* [Lab 4: Enable, configure, and run License Compliance](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/secessentialshandson4.html)
* [Lab 5: Enable, configure, and run Coverage-Guided Fuzz Testing](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/secessentialshandson5.html)
